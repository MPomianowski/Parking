package com.gitlab.mpomianowski.parking.controllers;

import com.gitlab.mpomianowski.parking.dto.Pair;
import com.gitlab.mpomianowski.parking.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/calc")
public class CalculatorControler {
    @Autowired
    public CalculatorControler(CalculatorService calculator) {
        this.calculator = calculator;
    }
    CalculatorService calculator;

    @RequestMapping(value = "/square/{x}", method = RequestMethod.GET)
    public int square(@PathVariable int x){

        return calculator.square(x);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public int add(@RequestBody Pair numbers){

        return calculator.add(numbers.getX(), numbers.getY());

    }

}
