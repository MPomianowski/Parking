package com.gitlab.mpomianowski.parking.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class HelloWolrdController {


    @RequestMapping(value = "/fb")
    public String fakajBobla(){
        return "fakajBobla";
    }
    @RequestMapping(value = "/data")
    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String data = dateFormat.format(date);
        log.info(data);
        return data;
    }

}
