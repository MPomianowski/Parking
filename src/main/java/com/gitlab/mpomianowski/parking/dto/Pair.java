package com.gitlab.mpomianowski.parking.dto;

import lombok.Data;

@Data
public class Pair {
    private int x;
    private int y;

}
