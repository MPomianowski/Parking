package com.gitlab.mpomianowski.parking.service;

import org.springframework.stereotype.Component;

@Component
public class CalculatorService {

    public int square(int x){
        return x*x;
    }

    public int add(int x, int y){
        return x+y;
    }

}
